const http = require('http'),
    dns = require('dns'),
    os = require('os');

let lastMessage = "";
const port = {};
port.reflected = 8000;

port.stored = 9000;

port.message = 9001;

http.createServer(function (request, response) {

    response.writeHeader(200, {"Content-Type": "text/html"});
    response.write("<html><head><meta charset=\"UTF-8\">\n</head><body align=\"center\">");
    response.write("<h1>Willkommen zu dieser Demo!</h1>");
    response.write("<h3>Die aktuelle Seite heißt: " + decodeURI(request.url.substr(1)) + ".</h3>");
    response.write("<div> Diese Demo ist erreichbar unter ");
    dns.lookup(os.hostname(), function (err, add, fam) {
        response.write(add + ":" + port.reflected + "</div>");
        response.write("<img src=\"http://api.qrserver.com/v1/create-qr-code/?data=http://" + add + ":" + port.reflected + "\"/>");
        response.write("</body></html>");
        response.end();
    });
}).listen(port.reflected);

http.createServer(function (request, response) {

    response.writeHeader(200, {"Content-Type": "text/html"});
    response.write("<html><head><meta charset=\"UTF-8\">\n</head><body align=\"center\">");
    response.write("<h1>Willkommen zu dieser Demo!</h1>");
    response.write("<h3>Die hinterlassene Nachricht lautet: " + lastMessage + ".</h3>");
    response.write("<div>Nachricht hinterlassen: </div><input type='text' id='message' placeholder='Deine Nachricht'>");

    response.write("<div> Diese Demo ist erreichbar unter ");
    dns.lookup(os.hostname(), function (err, add, fam) {
        response.write(add + ":" + port.stored + "</div>");
        response.write("<img src=\"http://api.qrserver.com/v1/create-qr-code/?data=http://" + add + ":" + port.stored + "\"/>");
        response.write("<script>var inText; inText = document.getElementById('message');" +
            "inText.onkeypress = function(e){" +
            "if (!e) e = window.event;\n" +
            "let keyCode = e.keyCode || e.which;" +
            "if (keyCode == '13'){" +
            "  const request = new XMLHttpRequest();" +
            "let url = 'http://'+'" + add + ":" + port.message + "/'+inText.value;" +
            "console.log(url);" +
            "        request.open(\"GET\", url, true);" +
            "request.send();" +
            "      return false;" +
            "    }" +
            "  }</script>"
        )
        ;
        response.write("</body></html>");
        response.end();
    });
}).listen(port.stored);

http.createServer(function (request, response) {

    lastMessage = decodeURI(request.url.substr(1));
    console.log(lastMessage);

    response.writeHeader(200, {'Access-Control-Allow-Origin': '*'});


    response.end();


}).listen(port.message);

http.createServer(function (request, response) {


    response.writeHeader(200, {'Access-Control-Allow-Origin': '*'});

    response.write("ok");

    response.end();


}).listen(8080);